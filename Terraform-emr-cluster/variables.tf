variable "Name" {}

variable "vpc_id" {}

variable "release_label" {}

variable "ebs_root_volume_size" {
}


variable "applications" {
 type    = "list"
}

variable "custom_ami_id" {}

variable "key_name" {}

variable "subnet_id" {}

variable "instance_groups" {
  default = [
    {
      name           = "MasterInstanceGroup"
      instance_role  = "MASTER"
      instance_type  = "m4.large"
      instance_count = 1
    },
    {
     name           = "TaskInstanceGroup"
     instance_role  = "TASK"
     instance_type  = "m4.large"
     instance_count = 1
     autoscaling_policy = <<EOF
{
 "Constraints": {
   "MinCapacity": 2,
   "MaxCapacity": 12
 },
 "Rules": [
 {
   "Name": "ScaleOutMemoryPercentage",
   "Description": "Scale out if YARNMemoryAvailablePercentage is less than 15",
   "Action": {
     "SimpleScalingPolicyConfiguration": {
       "AdjustmentType": "CHANGE_IN_CAPACITY",
       "ScalingAdjustment": 1,
       "CoolDown": 300
     }
   },
   "Trigger": {
     "CloudWatchAlarmDefinition": {
       "ComparisonOperator": "LESS_THAN",
       "EvaluationPeriods": 1,
       "MetricName": "YARNMemoryAvailablePercentage",
       "Namespace": "AWS/ElasticMapReduce",
       "Period": 300,
       "Statistic": "AVERAGE",
       "Threshold": 15.0,
       "Unit": "PERCENT"
      }
    }
  }
 ]
}
EOF
   },
    {
      name           = "CoreInstanceGroup"
      instance_role  = "CORE"
      instance_type  = "m4.large"
      instance_count = "1"
      autoscaling_policy = <<EOF
{
  "Constraints": {
    "MinCapacity": 2,
    "MaxCapacity": 12
  },
  "Rules": [
  {
    "Name": "ScaleOutMemoryPercentage",
    "Description": "Scale out if YARNMemoryAvailablePercentage is less than 15",
    "Action": {
      "SimpleScalingPolicyConfiguration": {
        "AdjustmentType": "CHANGE_IN_CAPACITY",
        "ScalingAdjustment": 1,
        "CoolDown": 300
      }
    },
    "Trigger": {
      "CloudWatchAlarmDefinition": {
        "ComparisonOperator": "LESS_THAN",
        "EvaluationPeriods": 1,
        "MetricName": "YARNMemoryAvailablePercentage",
        "Namespace": "AWS/ElasticMapReduce",
        "Period": 300,
        "Statistic": "AVERAGE",
        "Threshold": 15.0,
        "Unit": "PERCENT"
       }
     }
   }
  ]
}
EOF
    },
  ]
  type = "list"
}

variable "tags" {
  type = "map"
  default = {}
}
