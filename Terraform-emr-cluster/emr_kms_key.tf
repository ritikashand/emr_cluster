#
## Create KMS Key for EMR Encryption
#

resource "aws_kms_key" "emr_kms_key" {
  description             = "KMS key for EMR EC2 instances encryption"
  policy = "${file("emr_kms_policy.json")}"
  deletion_window_in_days = 30
}

resource "aws_kms_alias" "emr_kms_key" {
  name = "alias/auto-emr/s3_kms_key"
  target_key_id = "${aws_kms_key.emr_kms_key.key_id}"
}
