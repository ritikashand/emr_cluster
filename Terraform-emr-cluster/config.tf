# Configuration of the terraform state file backend and the DynamoDB lock table
# Need to initialize terraform backend like this example:
# terraform init -backend-config="key=mac-custom-ami-key/terraform.tfstate"
# need to use dynamodb for locking state dynamodb_table = "tf-locks-us-east-1"

terraform {
  backend "s3" {
    bucket  = "sb-mac-platformconfig-us-east-1"
    key     = "mac-emr-sb-a1/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
