## Create IAM Role for EMR--->auto_emr_role
#

data "aws_iam_policy_document" "emr_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["elasticmapreduce.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "AmazonElasticMapReduceRole" {
  name               = "auto_emr_role"
  assume_role_policy = "${data.aws_iam_policy_document.emr_assume_role.json}"
}


##Create policy for EMR Disk Encryption

resource "aws_iam_policy" "emr-kms-disk-encryption" {
    name        = "emr-kms-disk-encryption"
    policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
          {
              "Sid": "EmrDiskEncryptionPolicy",
              "Effect": "Allow",
              "Action": [
                  "kms:Encrypt",
                  "kms:Decrypt",
                  "kms:ReEncrypt*",
                  "kms:CreateGrant",
                  "kms:GenerateData",
                  "kms:GenerateDataKeyWithoutPlaintext",
                  "kms:DescribeKey"
              ],
              "Resource": [
                  "*"
              ]
          }
      ]
}
EOF
}


## Create policy for EMR Full Access

resource "aws_iam_policy" "emr_fullaccess" {
    name        = "auto_emr_fullaccess"
    policy = <<EOF
{
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "VisualEditor0",
                "Effect": "Allow",
                "Action": "elasticmapreduce:*",
                "Resource": "*"
            }
      ]
}
EOF
}

## Attaching the policies above to the EMR Role
# 3 policies attached. 1 - managed aws policy, 2 inline policies

resource "aws_iam_role_policy_attachment" "attach_emr_map_reduce_policy" {
role       = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceRole"
}

resource "aws_iam_role_policy_attachment" "attach_emr_kms_encryption_policy" {
role       = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
policy_arn = "${aws_iam_policy.emr-kms-disk-encryption.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_emr_full_access_policy" {
role       = "${aws_iam_role.AmazonElasticMapReduceRole.name}"
policy_arn = "${aws_iam_policy.emr_fullaccess.arn}"
}



#
## IAM role for EC2 Instance Profile--->auto_emr_ec2_role
#


data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "AmazonElasticMapReduceforEC2Role" {
  name               = "auto_emr_ec2_role"
  assume_role_policy = "${data.aws_iam_policy_document.ec2_assume_role.json}"
}


## Attaching the Disk Encryption and Full Access Policies to EC2 Instance Profile Role


resource "aws_iam_role_policy_attachment" "attach_ec2_map_reduce_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforEC2Role"
}


resource "aws_iam_role_policy_attachment" "attach_ec2_kms_encryption_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  policy_arn = "${aws_iam_policy.emr-kms-disk-encryption.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_ec2_full_access_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  policy_arn = "${aws_iam_policy.emr_fullaccess.arn}"
}



#
## Create auto scaling role--->auto_emr_autoscale_role
#


data "aws_iam_policy_document" "autoscale_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "AmazonElasticMapReduceforAutoScalingRole" {
  name               = "auto_emr_autoscale_role"
  assume_role_policy = "${data.aws_iam_policy_document.autoscale_assume_role.json}"
}


## Attaching the Amazon Elastic Map Reduce Policy to the Role

resource "aws_iam_role_policy_attachment" "attach_autoscale_map_reduce_policy" {
  role       = "${aws_iam_role.AmazonElasticMapReduceforAutoScalingRole.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceforAutoScalingRole"
}


#
## Create IAM Instance profile
#

resource "aws_iam_instance_profile" "auto_emr_ec2_instance_profile" {
  name = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
  role = "${aws_iam_role.AmazonElasticMapReduceforEC2Role.name}"
}
