#
## Create Security Group for Master Node
#

resource "aws_security_group" "emr_master" {
  vpc_id                 = "${var.vpc_id}"
  description            = "Security Group for Master Node in EMR Cluster"
  revoke_rules_on_delete = true

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags,map("Name","${var.Name}_Master_SG"))}"
}


#
## Create Security Group for Slave Node
#


resource "aws_security_group" "emr_slave" {
  vpc_id                 = "${var.vpc_id}"
  description            = "Security Group for Slave (core and task) Nodes in EMR Cluster"
  revoke_rules_on_delete = true

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags,map("Name","${var.Name}_Slave_SG"))}"

}
