#output file
#values outputted on the comand line

output "vpc_id" {
  value = "${var.vpc_id}"
}

output "name" {
  value = "${var.Name}"
}

output "master_securitygroup_id" {
  value = "${aws_security_group.emr_master.id}"
}

output "slave_securitygroup_id" {
  value = "${aws_security_group.emr_slave.id}"
}
