#
## Create Cluster with values needed
#

resource "aws_emr_cluster" "cluster" {
  name           = "${var.Name}"
  release_label  = "${var.release_label}"
  applications   = "${var.applications}"
  custom_ami_id  = "${var.custom_ami_id}"
  autoscaling_role = "${aws_iam_role.AmazonElasticMapReduceforAutoScalingRole.arn}"
  service_role = "${aws_iam_role.AmazonElasticMapReduceRole.arn}"
  instance_group = "${var.instance_groups}"
  security_configuration="${aws_emr_security_configuration.emr_sc_encryption.name}"
  log_uri="s3://aws-logs-081417463982-us-east-1/elasticmapreduce/"
  ebs_root_volume_size = "${var.ebs_root_volume_size}"



 ec2_attributes {
    key_name                          = "${var.key_name}"
    subnet_id                         = "${var.subnet_id}"
    emr_managed_master_security_group = "${aws_security_group.emr_master.id}"
    emr_managed_slave_security_group  = "${aws_security_group.emr_slave.id}"
    instance_profile                 = "${aws_iam_instance_profile.auto_emr_ec2_instance_profile.arn}"
  }

  tags = "${merge(var.tags,map("Name","${var.Name}"))}"

 }
