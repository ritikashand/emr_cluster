
Name          = "Auto_EMR_Cluster-04182018-718p"
vpc_id        = "vpc-25046c5e"
subnet_id     = "subnet-a1f445ae"
release_label = "emr-5.12.1"
applications  = ["Hadoop","Hive","Hue","Pig"]
#applications = ["Spark","Hadoop","Pig","Hue","Zeppelin","Hive", "HCatalog","HBase","Presto","Tez","ZooKeeper",
#                "Sqoop","Phoenix","Flink","Mahout","Oozie","Livy","Ganglia","MXNet"]
ebs_root_volume_size="20"
custom_ami_id = "ami-7e8d2001"
key_name     = "auto_emr_ssh_key"

tags={
  Project     = "Auto-EMR-Cluster"
  Environment = "Mac-SB-EMR-Cluster"
  Department ="MAC"
  Requestor  ="rs256x"
}
