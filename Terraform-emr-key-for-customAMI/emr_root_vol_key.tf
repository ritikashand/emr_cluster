provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_kms_key" "ami_key" {
  description             = "KMS key for Custom EMR AMI"
  policy = "${file("emr_kms_key_policy.json")}"
  deletion_window_in_days = 30
}

resource "aws_kms_alias" "ami_key" {
  name = "alias/auto-emr/ami_kms_key"
  target_key_id = "${aws_kms_key.ami_key.key_id}"
}
